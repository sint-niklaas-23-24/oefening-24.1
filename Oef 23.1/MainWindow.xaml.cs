﻿using System;
using System.Windows;

namespace Oef_23._1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        Dobbelsteen Dobbel1 = new Dobbelsteen(4, new Random());
        Dobbelsteen Dobbel2 = new Dobbelsteen(new Random());

        public void btnGooien_Click(object sender, RoutedEventArgs e)
        {
            Dobbel1.Roll();
            Dobbel2.Roll();

            lblBlauw.Content = Dobbel1.Waarde;
            lblRood.Content = Dobbel2.Waarde;
        }
    }
}
